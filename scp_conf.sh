for node in node-1 node-2 node-3; do
    scp /home/hadoop/hadoop/etc/hadoop/* hadoop@$node:/home/hadoop/hadoop/etc/hadoop;
    scp /home/hadoop/zookeeper/conf/zoo.cfg hadoop@$node:/home/hadoop/zookeeper/conf/;
    scp /home/hadoop/apache-drill/conf/drill-override.conf hadoop@$node:/home/hadoop/apache-drill/conf;
done
