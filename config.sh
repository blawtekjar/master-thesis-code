#!/bin/bash

username="hadoop"

egrep "^$username" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "$username user already exists!"
else
	pass=$(perl -e 'print crypt($ARGV[0], "password")' $1)
	sudo useradd -m -p $pass $username
	sudo sh -c "sudo echo '$username  ALL=(ALL:ALL) ALL' >> /etc/sudoers"
fi

sudo apt-get update
sudo apt-get install openjdk-8-jdk -y

sudo egrep "gateway" /etc/hosts >/dev/null
if [ $? -eq 0 ]; then
        echo "Hostname settings probably OK, skipping /etc/hosts edit"
else
	echo "exporting hostnames"
	sudo sh -c "echo '10.0.19.12 gateway' >> /etc/hosts"
	sudo sh -c "echo '10.0.19.13 node-3' >> /etc/hosts"
	sudo sh -c "echo '10.0.19.15 node-2' >> /etc/hosts"
	sudo sh -c "echo '10.0.19.14 node-1' >> /etc/hosts"
fi
echo "Done!"

sudo su -c ./config2.sh -s /bin/bash hadoop
