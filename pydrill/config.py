#!/usr/bin/python3

import sys
import class_definition
from plotting import plot_generation

# Variables that define the function 
CONCURRENT_RUNS = 1
POPULATION_SIZE = 10
GROWTH_FACTOR = 0.2
GROWTH_CHANCE = 0.2
REPLACEMENTS_EACH_RUN = 4
GENERATIONS = 10

# Variables that define the cluster
DATA_SOURCE = "drill64"
NODES = 4
CORES = 4

# Variables that define what the user sees
DISPLAY_OUTPUT = False


# Check CLI args
for arg in sys.argv:
	if arg == "-display":
		DISPLAY_OUTPUT = True
	if arg.startswith("-concurrent"):
		mylist = arg.split(":")
		CONCURRENT_RUNS = int(mylist[1])

judge = class_definition.Judge(POPULATION_SIZE,NODES,CORES,DATA_SOURCE,CONCURRENT_RUNS,DISPLAY_OUTPUT,GROWTH_FACTOR,GROWTH_CHANCE)
for x in range(GENERATIONS):
	judge.run_candidates()
	judge.evolve_population(REPLACEMENTS_EACH_RUN)

judge.run_candidates()
plot_generation(judge.generations)

#candidate = class_definition.Base_candidate(class_definition.Setting(NODES,CORES))
#candidate.apply_settings_for_session(candidate.settings.core_settings,DATA_SOURCE)
#candidate.run(CONCURRENT_RUNS,DISPLAY_OUTPUT,DATA_SOURCE)
