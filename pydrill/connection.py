import pyodbc
import timeit
from random import randint

# Start timed snippet
def connect_z(color,dsn):
	start_connect = timeit.default_timer()
	conn = pyodbc.connect("DSN="+dsn, autocommit=True)
	cursor = conn.cursor()
	# End timed snippet
	connect_time = timeit.default_timer() - start_connect
	output = color.OKGREEN+"\n### RESULT ###"
	output = output+"\nODBC connection drill64 Zookeper quorum: "+ str(connect_time)+" s\n"
	#print(output)
	return connect_time,cursor

def execute_q(connect_time,cursor,dsn,color,display_settings,index,avg_times):
	# Global array for storing run times from threads
	# This may not be set, before summarizing, so we initialize it as 0 here.
	display_time = 0
	
	### perform the query
	# Start timed snippet
	start_query = timeit.default_timer()
	# setup the query and run it

	# TODO: Fix some querying logic

	# FROM YELP SET
	s = '''SELECT Business, Days, Total_Checkins
			FROM (SELECT Business, Days, SUM(tot1.Checkins.`value`) as Total_Checkins
			FROM (SELECT tot.name as Business, tot.TIMES.key as Days, FLATTEN(KVGEN(tot.TIMES.`value`)) as Checkins
			FROM (SELECT FLATTEN(KVGEN(tbl.`time`)) as TIMES, business.name
			FROM (SELECT * FROM `/user/hadoop/drill/yelp_data/checkin.json` LIMIT 10) as tbl
			JOIN `/user/hadoop/drill/yelp_data/business.json` as business
			ON business.business_id=tbl.business_id) as tot) as tot1
			GROUP BY Business,Days) WHERE Total_Checkins>150 ORDER BY Business,Total_Checkins'''

	# FROM ENRON SET
	#s = "SELECT * FROM hdfs.`/user/hadoop/drill/` WHERE dir0='maildir' AND dir2='deleted_items' AND columns[0] LIKE '%lawsuit%'"

	# FROM POKEMON SET
	#cursor.execute("SET `store.json.all_text_mode`=true") # The pokemon set needs this, make some exception handler to automatically detect depencancy?
	#pnumber = randint(0, 151)
	#s = "SELECT pokemon["+str(pnumber)+"].name as Pokemon FROM `/user/hadoop/drill/pokedex.json`"

	# FROM BASEBALL SET
	#cursor.execute("SELECT * FROM `/user/hadoop/drill/baseball/Master.csv`")

	# Query
	#print(color.WARNING+"Executing query:\n"+s+color.ENDC)
	cursor.execute("USE hdfs")
	cursor.execute(s)

	# End timed snippet
	query_time = timeit.default_timer() - start_query
	output = color.ENDC+"\nQuery execution: "+color.WARNING+ str(query_time)+" s"+color.OKGREEN

	### Display the results
	if display_settings:
		# Start timed snippet
		start_display = timeit.default_timer()
		rows = cursor.fetchall()
		for row in rows:
	    		print(row)
		# End timed snippet
		display_time = timeit.default_timer() - start_display
		output = output+"\nDisplaying results: "+ str(display_time)+" s"


	total_time = connect_time+query_time+display_time
	output = output+"\nTotal test time: "+ str(total_time)+" s\n"+color.ENDC
	#pnumberrint(output)
	avg_times[index] = query_time

def execute_query_sequence(sequence,cursor,color):
	for query in sequence:
		#print(color.WARNING+"Executing query:\n"+query+color.ENDC)
		cursor.execute(query)
