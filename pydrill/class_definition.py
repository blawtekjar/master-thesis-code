
import random, sys
from threading import Thread
from statistics import mean
from connection import connect_z, execute_q, execute_query_sequence
from color import Color

class Judge:

	def __init__(self,pop_size,nodes,cores,dsn,conc,disp,growth_factor,growth_chance):
		self.dsn = dsn
		self.conc = conc
		self.disp = disp
		self.nodes = nodes
		self.cores = cores

		self.growth_chance = growth_chance
		self.growth_factor = growth_factor
		self.population_size = pop_size
		self.default_candidate = Base_candidate(Setting(self.nodes,self.cores),name="Default candidate")

		self.define_goal(self.default_candidate)
		self.generate_population()

		self.generation = 1
		self.generations = {}
		upper = []
		lower = []
		mean = []
		self.generations["upper"] = upper
		self.generations["lower"] = lower
		self.generations["mean"] = mean

	def define_goal(self,candidate):
		candidate.run(self.conc,self.disp,self.dsn)
		self.mean_goal = candidate.mean_query * 0.5 # Quite arbitrary, but set for testing purposes

	def evaluate_fitness(self,candidate):
		candidate.fitness = self.mean_goal / candidate.mean_query

	def generate_population(self):
		print("Generating original population")
		candidate_cage = []
		for x in range(self.population_size):
			bc = Base_candidate(Setting(self.nodes,self.cores),name="Candidate: "+str(x))
			permutations = random.randint(0, 100)
			if permutations > 0:
				for x in range(permutations):
					bc.mutate_settings(self.growth_factor,self.growth_chance)
			candidate_cage.append(bc)
		self.candidate_cage = candidate_cage

	def run_candidates(self):
		print("Testing generation "+str(self.generation))
		for c in self.candidate_cage:
			c.run(self.conc,self.disp,self.dsn)
			self.evaluate_fitness(c)
		for c in self.candidate_cage:
			print(c.result)
			print(str(c.mean_query)+", fitness: "+str(c.fitness))

	def execute_weakest(self,executions):
		for x in range(executions):
			lowest_fitness = 1
			for c in self.candidate_cage:
				if c.fitness < lowest_fitness:
					lowest_fitness = c.fitness
					weak_link = c
			print("Killing off "+weak_link.name)
			self.candidate_cage.remove(weak_link)

	def breed_strongest(self,births):
		highest_fitness = 0
		for c in self.candidate_cage:
			if c.fitness > highest_fitness:
				highest_fitness = c.fitness
				parent_a = c
		self.candidate_cage.remove(c) # Removing parent_a to also find the second best.
		highest_fitness = 0
		for c in self.candidate_cage:
			if c.fitness > highest_fitness:
				highest_fitness = c.fitness
				parent_b = c
		for x in range(births):
			print("Adding new crossed candidate")
			self.candidate_cage.append(Crossed_candidate(parent_a,parent_b,name="Crossed_candidate "+str(self.generation)+"."+str(x)))
		self.candidate_cage.append(parent_a) # Adding parent_a back.

	def evolve_population(self,replacements):
		upper = []
		lower = []
		mean = []
		for c in self.candidate_cage:
			self.generations["upper"].append(c.max_query)
			self.generations["lower"].append(c.min_query)
			self.generations["mean"].append(c.mean_query)
			
		self.execute_weakest(replacements)
		self.breed_strongest(replacements)
		for c in self.candidate_cage:
			c.mutate_settings(self.growth_factor,self.growth_chance)
		self.generation += 1

class Candidate:

	def __init__(self,name):
		self.name = name

	# TODO: The mutation, which is extremely vital for this solution to work
	# Needs a lot more work. This is the fundamental setup
	def mutate_settings(self,growth_factor,growth_chance):
		for setting in self.settings.core_settings:
			if setting["type"] != "boolean":
				if random.random() < growth_chance:
					if random.random() > 0.5:
						setting["query_value"] = setting["query_value"] * (1 - growth_factor)
					else:
						setting["query_value"] = setting["query_value"] * (1 + growth_factor)
				if setting["query_value"] > setting["range"]["max"]:
					setting["query_value"] = setting["range"]["max"]
				if setting["query_value"] < setting["range"]["min"]:
					setting["query_value"] = setting["range"]["min"]
		
		for setting in self.settings.extended_settings:
			if setting["type"] != "boolean" and setting["name"] != "planner.memory.non_blocking_operators_memory":
				if random.random() < growth_chance:
					if random.random() > 0.5:
						setting["query_value"] = setting["query_value"] * (1 - growth_factor)
					else:
						setting["query_value"] = setting["query_value"] * (1 + growth_factor)
				if setting["query_value"] > setting["range"]["max"]:
					setting["query_value"] = setting["range"]["max"]
				if setting["query_value"] < setting["range"]["min"]:
					setting["query_value"] = setting["range"]["min"]
			elif setting["name"] == "planner.memory.non_blocking_operators_memory":
				if random.random() < growth_chance:
					if random.random() > 0.5:
						setting["query_value"] = setting["query_value"] * 2
					else:
						setting["query_value"] = setting["query_value"] / 2
				if setting["query_value"] > setting["range"]["max"]:
					setting["query_value"] = setting["range"]["max"]
				if setting["query_value"] < setting["range"]["min"]:
					setting["query_value"] = setting["range"]["min"]

	def apply_settings(self,settings,dsn,scope="SESSION"):
		c = Color()
		connect_time, cursor = connect_z(c,dsn)
		sequence = []
		for setting in settings.core_settings:
			if setting["type"] == "double":
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(float(setting["query_value"])))
			elif setting["type"] == "integer":
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(int(setting["query_value"])))
			else:
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(setting["query_value"]))
		for setting in settings.extended_settings:
			if setting["type"] == "double":
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(float(setting["query_value"])))
			elif setting["type"] == "integer":
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(int(setting["query_value"])))
			else:
				sequence.append("ALTER "+scope+" SET `"+setting["name"]+"` = "+str(setting["query_value"]))
		execute_query_sequence(sequence,cursor,c)
		return connect_time,cursor, c

	def run(self,conc,disp,dsn):
		print("Running "+self.name)
		self.result = [None] * (conc * 2)
		self.threads = []
		for x in range(conc):
			self.connect_time,self.cursor,self.c = self.apply_settings(self.settings,dsn)
			self.process = Thread(target=execute_q, args=[self.connect_time,self.cursor,dsn,self.c,disp,x,self.result])
			self.process.start()
			self.threads.append(self.process)
		for process in self.threads:
			process.join()

		for x in range(conc,conc*2):
			execute_q(self.connect_time,self.cursor,dsn,self.c,disp,x,self.result)

		self.max_query = max(self.result)
		self.min_query = min(self.result)
		self.mean_query = mean(self.result)
		#print("\nLongest query time was "+str(self.max_query)+" s")
		#print("\nShortest query time was "+str(self.min_query)+" s")
		#print("\nAverage query time was "+str(self.mean_query)+" s\n")

class Crossed_candidate(Candidate):

	def generate_set(self,set1,set2):
		new_set = []
		for x in range(len(set1)):
			if random.random() > 0.5:
				new_set.append(set1[x])
			else:
				new_set.append(set2[x])		
		return new_set

	def emerge(self,candidate1,candidate2):
		setting1 = candidate1.settings
		setting2 = candidate2.settings
		new_core = self.generate_set(setting1.core_settings,setting2.core_settings)
		new_extend = self.generate_set(setting1.extended_settings,setting2.extended_settings)
		new_exception = self.generate_set(setting1.exception_settings,setting2.exception_settings)
		setting1.core_settings = new_core
		setting1.extended_settings = new_extend
		setting1.exception_settings = new_exception
		return setting1 # Just to avoid creating a new Setting object, we overwrite this one.

	def __init__(self,candidate1,candidate2,name="crossed_candidate"):
		super().__init__(name)
		self.settings = self.emerge(candidate1,candidate2)

class Base_candidate(Candidate):

	def set_system_defaults(nodes,cores,dsn):
		super().apply_settings(Setting(nodes,cores),dsn,scope="SYSTEM")

	def __init__(self,default_setting,name="candidate"):
		super().__init__(name)
		self.settings = default_setting


class Setting:

	def __init__(self,nodes,cores):

		maxint = int(sys.maxsize)
		#For core_setting21
		#number of active drillbits (nodes) * number of cores per node * 0.7
		calculated = int(nodes*cores*0.7)

		#Mentioned in Performance Tuning Section of official documentation
		#Noted as core options here:

		core_setting1 = {"name":"planner.enable_multiphase_agg","query_value":True,"type":"boolean","confidence":0.5} #Range: False/True
		core_setting2 = {"name":"planner.enable_broadcast_join","query_value":True,"type":"boolean","confidence":0.5} #Range: False/True
		core_setting3 = {"name":"planner.enable_hashagg","query_value":True,"type":"boolean","confidence":0.5} #Range: False/True
		core_setting4 = {"name":"planner.enable_hashjoin","query_value":True,"type":"boolean","confidence":0.5}	#Range: False/True
		core_setting5 = {"name":"planner.memory.enable_memory_estimation","query_value":False,"type":"boolean","confidence":0.5} #Range: False/True
		core_setting6 = {"name":"exec.queue.enable","query_value":False,"type":"boolean","confidence":0.5} #Range: False/True

		core_setting7 = {"name":"planner.broadcast_factor","query_value":1,"type":"double","range":{"min":0,"max":100}} #Range: 0-17976931348623157e+308 RANGE SET TO 100 FOR TESTING
		core_setting8 = {"name":"planner.broadcast_threshold","query_value":10000000,"type":"integer","range":{"min":0,"max":2147483647}} #Range: 0-2147483647
		core_setting9 = {"name":"planner.slice_target","query_value":1000,"type":"integer","range":{"min":0,"max":maxint}} #Range: integer
		core_setting10 = {"name":"planner.width.max_per_query","query_value":1000,"type":"integer","range":{"min":0,"max":maxint}} #Range: integer. note, only on big clusters
		core_setting11 = {"name":"exec.min_hash_table_size","query_value":65536,"type":"integer","range":{"min":0,"max":1073741824}} #Range: 0 to 1073741824.
		core_setting12 = {"name":"exec.max_hash_table_size","query_value":1073741824,"type":"integer","range":{"min":0,"max":1073741824}} #Range: 0 to 1073741824.
		core_setting13 = {"name":"exec.queue.large","query_value":10,"type":"integer","range":{"min":1,"max":1000}} #Range: 0-1000
		core_setting14 = {"name":"exec.queue.small","query_value":100,"type":"integer","range":{"min":0,"max":1073741824}} #Range: 0 to 1073741824.
		core_setting15 = {"name":"exec.queue.threshold","query_value":30000000,"type":"integer","range":{"min":0,"max":9223372036854775807}} #Range: 0-9223372036854775807
		core_setting16 = {"name":"exec.queue.timeout_millis","query_value":300000,"type":"integer","range":{"min":0,"max":9223372036854775807}}	#Range: 0-9223372036854775807
		
		# CANT BE CHANGED IN SESSION
		#core_setting17 = {"name":"exec.queue.memory_ratio","default":10,"type":"integer","range":{"min":0,"max":1}} #Range: integer 
		#core_setting18 = {"name":"exec.queue.memory_reserve_ratio","default":0.2,"type":"double","range":{"min":0,"max":1}} #Range: 0-1

		core_setting19 = {"name":"planner.memory.max_query_memory_per_node","query_value": 2147483648,"type":"integer","range":{"min":0,"max":maxint}} # bytes (2GB)
		core_setting20 = {"name":"planner.width.max_per_node","query_value":calculated,"type":"integer","range":{"min":0,"max":maxint}}

		# There are PLENTY of other options not mentioned as performance tuning variables here
		#https://drill.apache.org/docs/configuration-options-introduction/
		#Noted as extended options here:
		extended_setting1 = {"name":"planner.add_producer_consumer","query_value":False,"type":"boolean","confidence":0.5} #Range: False/True
		extended_setting2 = {"name":"planner.enable_hashjoin_swap","query_value":True,"type":"boolean","confidence":0.5} #Range: False/True
		extended_setting3 = {"name":"planner.enable_mergejoin","query_value":True,"type":"boolean","confidence":0.5} #Range: False/True
		extended_setting9 = {"name":"planner.memory.enable_memory_estimation","query_value":False,"type":"boolean","confidence":0.5} #Range: False/True

		extended_setting4 = {"name":"planner.filter.max_selectivity_estimate_factor","query_value":1,"type":"double","range":{"min":0,"max":1}} #Range: 0-1
		extended_setting5 = {"name":"planner.filter.min_selectivity_estimate_factor","query_value":0,"type":"double","range":{"min":0,"max":1}} #Range: 0-1 (but less than max?)
		extended_setting6 = {"name":"planner.join.hash_join_swap_margin_factor","query_value":10,"type":"double","range":{"min":0,"max":maxint}} #Range: Integer
		extended_setting7 = {"name":"planner.join.row_count_estimate_factor","query_value":1,"type":"double","range":{"min":0,"max":maxint}} #Range: Integer
		extended_setting8 = {"name":"planner.memory.average_field_width","query_value":8,"type":"integer","range":{"min":1,"max":maxint}} #Range: Integer
		extended_setting10 = {"name":"planner.memory.hash_agg_table_factor","query_value":1.1,"type":"double","range":{"min":0,"max":maxint}} #Range: Double?
		extended_setting11 = {"name":"planner.memory.hash_join_table_factor","query_value":1.1,"type":"double","range":{"min":0,"max":maxint}} #Range: Double
		extended_setting12 = {"name":"planner.memory.non_blocking_operators_memory","query_value":64,"type":"integer","range":{"min":2,"max":2048}} #Range: 0-2048, SPECIAL CASE, NEEDS TO BE A POWER OF 2
		extended_setting13 = {"name":"planner.partitioner_sender_max_threads","query_value":8,"type":"integer","range":{"min":0,"max":maxint}} #Range: Integer
		extended_setting14 = {"name":"planner.nestedloopjoin_factor","query_value":100,"type":"double","range":{"min":0,"max":maxint}} #Range: Integer
		extended_setting15 = {"name":"planner.producer_consumer_queue_size","query_value":10,"type":"integer","range":{"min":0,"max":maxint}} #Range: Integer
		extended_setting16 = {"name":"store.text.estimated_row_size_bytes","query_value":100,"type":"double","range":{"min":0,"max":maxint}} #Range: Integer

		# If there are errors it may come from schema confusion for the engine, solution are usually these
		#Noted as exception options here:
		exception_setting1 = {"name":"store.json.all_text_mode","query_value":False,"type":"boolean"} #Range: False/True
		exception_setting2 = {"name":"store.json.read_numbers_as_double","query_value":False,"type":"boolean"} #Range: False/True

		core_settings = [core_setting1,
						core_setting2,
						core_setting3,
						core_setting4,
						core_setting5,
						core_setting6,
						core_setting7,
						core_setting8,
						core_setting9,
						core_setting10,
						core_setting11,
						core_setting12,
						core_setting13,
						core_setting14,
						core_setting15,
						core_setting16,
						#core_setting17,
						#core_setting18,
						core_setting19,
						core_setting20]

		extended_settings = [extended_setting1,
							extended_setting2,
							extended_setting3,
							extended_setting4,
							extended_setting5,
							extended_setting6,
							extended_setting7,
							extended_setting8,
							extended_setting9,
							extended_setting10,
							extended_setting11,
							extended_setting12,
							extended_setting13,
							extended_setting14,
							extended_setting15,
							extended_setting16]

		exception_settings = [exception_setting1,
							exception_setting2]

		self.core_settings = core_settings
		self.extended_settings = extended_settings
		self.exception_settings = exception_settings