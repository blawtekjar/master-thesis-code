#!/bin/bash

cd $HOME

echo "Downloading Hadoop 3.0.0"
wget http://apache.uib.no/hadoop/common/hadoop-3.0.0/hadoop-3.0.0.tar.gz
tar xzf hadoop-3.0.0.tar.gz
mv hadoop-3.0.0 hadoop
rm hadoop-3*
echo "Download complete."

egrep "^export JAVA_HOME" .bashrc >/dev/null
if [ $? -eq 0 ]; then
	echo "Path settings probably OK, skipping bashrc edit"
else
	echo "exporting environmentals"
	echo "export PATH=/home/hadoop/hadoop/bin:/home/hadoop/hadoop/sbin:$PATH" >> .bashrc
	echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> .bashrc
	echo "export LANGUAGE=en_US.UTF-8" >> .bashrc
	echo "export LC_ALL=en_US.UTF-8" >> .bashrc
fi

echo "Done!"
